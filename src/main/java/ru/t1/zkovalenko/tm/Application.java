package ru.t1.zkovalenko.tm;

import ru.t1.zkovalenko.tm.constant.ArgumentsConst;
import ru.t1.zkovalenko.tm.constant.TerminalConst;

import java.util.Locale;
import java.util.Scanner;

public final class Application {

    public static void main(String[] args) {
        if (processArguments(args)) exit();

        System.out.println("** Welcome to Task-Manager **");

        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Enter Command:");
            final String command = scanner.nextLine();
            processArgument(command);
        }
    }

    private static void processArgument(final String arg) {
        switch (arg.toLowerCase(Locale.ENGLISH)) {
            case TerminalConst.VERSION:
            case ArgumentsConst.VERSION:
                showVersion();
                break;
            case TerminalConst.HELP:
            case ArgumentsConst.HELP:
                showHelp();
                break;
            case TerminalConst.ABOUT:
            case ArgumentsConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.EXIT:
                exit();
            default:
                showError();
        }
    }

    private static void exit() {
        System.exit(0);
    }

    private static boolean processArguments(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    public static void showError() {
        System.err.println("Command Not Found");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.6.0");
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Who is the best: Read below");
        System.out.println("Developer: 3axarko");
        System.out.println("And he's really good");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s, %s - Show app version \n", TerminalConst.VERSION, ArgumentsConst.VERSION);
        System.out.printf("%s, %s - Who did it? And why? \n", TerminalConst.ABOUT, ArgumentsConst.ABOUT);
        System.out.printf("%s, %s - App commands \n", TerminalConst.HELP, ArgumentsConst.HELP);
    }

}
